#Converting a Tomee Zapp Gun to an IR Duino Tag gun.

##Tools Needed:

  1) [Tomee Zapp Gun][1]
  2) 00-Philips Head Screw Driver
  3) Soldering Iron
  4) Tweezers
  5) Helping Hands or other tool to hold pieces when using soldering iron
  6) IR LED
  7) 220 Ohm resistor
  8) [Momentary push button][2]


##Steps

  1) Unscrew the 3x ~7.75mm screws from the grey greebling on the Zapp Gun

  2) Remove the greebling by pulling the two halves apart

  3) Unscrew the 2x ~3.75mm screws from the front of the Zapp Gun barrel

  4) Unscrew the 2x ~7.75mm screws from the Zapp Gun handle

  5) Gently pull the two halfs of the Zapp Gun apart, being careful not to lose
  the lense mounted inside the barrel

  6) Carefully remove the sensor circuit board from within its mouting in the
  barrel

  7) Using the soldering iron and tweezers, desolder and remove the wiring and
  IR sensor from the sensor board

  8) Place an infrered LED in the same position as the sensor you removed from
  the board and solder the legs in place in the through hole

  9) Solder a 220 Ohm resistor to the positive leg of the 

  10) Solder the red wire from the Zapp Gun cable to the other end of the
  resistor

  11) Solder the black wire from the Zapp Gun cable to the middle position of
  the trigger switch. This will be our ground point.

  12) Solder another black wire from the negative leg of the LED to ground

  13) Solder the white wire from the Zapp Gun cable to the first (closest to
  the trigger) position on the trigger switch. When you pull the trigger on the
  gun, you should see the middle position move to make contact with the position
  tht the white wire is soldered to.

  14) Drill a small hole in the bottom of the Zapp Gun handle and mount the push
  button through it. I used some [Loctite PowerGrab][3] to glue the button body
  to the case which seemed to work ok, but something more secure might be better

  15) Solder the green wire from the Zapp Gun cable to one side of the button,
  you may need some additional jumper wire to make it reach

  16) Solder another black wire from the other side of the button to ground

  17) Replace the sensor board and lense in their respective holders, carefully
  rout the wiring to ensure nothing is pinched and re-assemble the two halves of
  the Zapp gun by following steps 1-5 in reverse order.



[1]:https://www.amazon.com/Tomee-Zapp-Gun-NES/dp/B000MEA9TQ/
[2]:https://www.amazon.com/Gikfun-12x12x7-3-Tactile-Momentary-Arduino/dp/B01E38OS7K/
[3]:https://www.amazon.com/Loctite-2029846-All-Purpose-Construction-Adhesive/dp/B005M21YQK