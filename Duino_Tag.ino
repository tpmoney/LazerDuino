/**
 * Arduino Tag game
 */

#include "MenuManager.h"
#include "DuinoTagGlobals.h"
#include "GameParameters.h"
#include "Utilities.h"

//Display
#include <LiquidCrystal.h>
//IRLib2
#include <IRLibRecvPCI.h>
#include <IRLibDecodeBase.h>
#include <IRLibSendBase.h>
#include <IRLib_P13_MilesTag.h>
#include <IRLibCombo.h>
//Audio
#include <DFRobotDFPlayerMini.h>
#include <SoftwareSerial.h>

//Personal debugger utility
#define DEBUG 1
#define DEBUG_IR_READ DEBUG && 0 //Only allowed if DEBUG is already 1


/* Possible use for Team ID LED 
const byte TEAM_COLOR[] = {0,0,0, 
  255,0,0, //Red
  0,0,255, //Blue
  255,255,0, //Yellow
  0,255,0, //Green
  192,192, 192, //Black
  255,255,255, //White
  128,0,128}; //Purple

 */



//-------------------------------GLOBAL VAR DECLARATIONS-----------------------------------------------------//
//Globals vars can't be defined in the .h file and
// still be included in other files cyclically so init
// them here
MenuManager menus;
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_DB4, LCD_DB5, LCD_DB6, LCD_DB7);
IRdecode irDecoder;
IRsend irSender;
IRrecvPCI irRemote(SENSOR);
DFRobotDFPlayerMini audioPlayer;
SoftwareSerial softSerial(AUDIO_RX, AUDIO_TX);

//Timing values
unsigned long gameStart             = 0; //Time when the game started, or the total game time when in GAME OVER
unsigned long disabledStart         = 0; //Time when the player was last disabled
unsigned long displayTimeout        = UINT32_MAX; //Time point when the display will turn off next
unsigned long buttonTimer           = 0; //Used for determining how long a button has been held for
byte disabledTimeout                = 0; //Seconds until the disabled status is removed
unsigned long chemicalBurnStart     = 0; //Time when the player last received chemical damage
byte chemicalBurnsApplied           = 0; //When receiving chemical damage, incremented to count the number of times damage is applied
unsigned long radiationPoisonStart  = 0; //Time when the player last received radiation damage
byte radiationPoisonApplied         = 0; //When receiving radiation damage, incremented to count the number of times damage is applied

//IR Processing
boolean readData                    = false; //Whether or not we successfully read valid data
byte numBits                        = 0;  //The number of bits received
uint32_t irValue                    = 0;  //The value of the IR message

//Flag vars
byte gameFlags                      = 0; //Game flags for various statuses
byte buttons                        = 0; //Bits are set to 1 when a button is being held

char progmemString[16];                  //Buffer for gettting strings out of program memory


//Same restriction on imports applies to the GameParameters

GameSettings GS = { //Default End User Configurable Parameters, if saved in EEPROM, these will be overwritten with the saved values
  100,  //Maximum Player Health
  100,  //Starting Player Health (also when revived)      
  5,    //The maximum number of clips of ammunition a player can have (set to 0 for infinite clips)
  2,    //Starting player ammo clips (in addition to starting with CLIP_SIZE ammo)
  10,   //The maximum amount of ammo a single clip will hold (set to 0 for infinite ammo without reloads)      
  3,    //The maximum amount of RPGs a player can have (set to 0 for infinite RPGs)
  1,    //Starting player RPGs     
  60,   //The max number of seconds a disable command can set (note this is note the total max disable time, see wait below)
  60,   //The number of seconds that must have ellapsed since the player was last disabled before another command will take effect
        //  By setting this to less than the max disable time, it's possible for a player to have a longer effective disable time
        //  than allowed by the MAX_DISABLE_TIME setting, up to 255 seconds which is the limit of the byte used to store the current
        //  disable timeout.      
  4,    //The amount of damage per second chemical weapons apply to the player
  3,    //The max number of seconds that a chemical weapon will continue apply damage to the player
  2,    //The amount of damage per second radiaition weapons apply to the player
  5,    //The max number of seconds that a rediation weapon will continue to apply damage to the player
  0    //The number of tenths of a second it takes to reload when a clip runs out
};

PlayerSettings PS = { //Default player settings, if saved in EEPROM, these will be overwritten with the saved values
  1,            //Invidiual player ID
  1,            //Player's Team ID
  25,           //Player's power level
  ROLE_PLAYER,  //This device's role
  GAME_A        //Which game we're playing in (for parity purposes)
};

byte myHealth;
byte myClips;
byte myAmmo;
byte myRPGs;
byte myScore;
byte myMedals;
byte myPenalties;
uint32_t myShotPacket;
//-----------------------------------End Global Var Declarations---------------------------------------//

void setup() {
  // put your setup code here, to run once:
  #if DEBUG
    Serial.begin(9600);
  #endif

  //Pin init
  pinMode(LED, OUTPUT);
  pinMode(SENSOR, INPUT);
  pinMode(TRIGGER, INPUT_PULLUP);
  pinMode(RELOAD, INPUT_PULLUP);
  pinMode(FUNCTION, INPUT_PULLUP);
  pinMode(IR_LED, OUTPUT);
  pinMode(LCD_BACKLIGHT, OUTPUT);
  digitalWrite(LCD_BACKLIGHT, HIGH);

  //LCD Init
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.print(F("Please Wait..."));

  softSerial.begin(9600);
  if(!audioPlayer.begin(softSerial)) {
    lcd.setCursor(0,0);
    lcd.print(F("Audio Error"));
    while(true) {
      digitalWrite(LED, HIGH);
      delay(200);
      digitalWrite(LED, LOW);
      delay(200);
    }
  }

  audioPlayer.volume(25);

  if(!digitalRead(FUNCTION) == LOW) {
    loadSettingsFromEEPROM();
  }

  initializePlayer();
  clearScores();
  updateShotPacket();

  gameFlags = 0; //Start in game mode
  gameStart = millis();
  irRemote.enableIRIn();

  playSuccess();
  printStatus();

  #if DEBUG
    //bbox.start(DEBUG_TRIGGER, DEBUG_PINS, sizeof(DEBUG_PINS) / sizeof(uint8_t));
    Serial.println(F("Ready..."));
  #endif
}

void loop() {
  // put your main code here, to run repeatedly:
  readData = false;
  numBits = 0;
  irValue = 0;

  //If we're in menu mode, jump to the menus
  if(gameFlags & FLAG_MENU_MODE) {
    menuMode();
  }

  //Check how long we've been holding function
  checkFunctionHold();

  //Check for incoming IR signals
  senseIR();

  //Check for a pulled trigger
  senseFire();

  //Check if the user is reloading
  senseReload();

  //If we're disabled, check if we're past the timeout
  if(disabledTimeout) {
    checkDisabledTimeout();
  }

  //If we're suffering chemical burns, apply ongoing damage
  if(chemicalBurnStart) {
    applyChemicalBurn();
  }

  //If we're suffering radiation poisoning, apply ongoing damage
  if(radiationPoisonStart) {
    applyRadiationPoison();
  }

  //Finally, check and see if we should turn off the display
  if(displayTimeout < millis()){
    turnDisplayOff();
  }
}

/**
 * Check if we've passed the timeout limit for being disabled
 */
void checkDisabledTimeout() {
  if(millis() - disabledStart > (disabledTimeout * SECONDS_TO_MILLIS)) {
    gameFlags ^= FLAG_DISABLED; //Turn off the disabled flag
    disabledTimeout = 0;
  }
}

/**
 * Check if data has been received on the IR sensor and
 * process it
 */
void senseIR() {
  if (irRemote.getResults()){
    readData = irDecoder.decode();
    numBits = irDecoder.bits;
    irValue = irDecoder.value;
    irRemote.enableIRIn();
    #if DEBUG_IR_READ
      irDecoder.dumpResults(true);
      Serial.println(irDecoder.value, BIN);
    #endif 
  }
  
  if(readData){
    
    #if DEBUG
      Serial.println(irValue, BIN);
      Serial.println(numBits);
    #endif

    if(validatePacket(irValue, numBits)) {
      #if DEBUG
        Serial.println(F("Valid data"));
      #endif
      
      processPacket((irValue >> 1));
    } else {
      #if DEBUG
        Serial.println(F("Invalid data"));
      #endif
    }
  }
  
}

/**
 * Process a valid data packet
 */
void processPacket(uint16_t dataPacket) {
  //We can just truncate the upper 8 bits with this cast
  byte secondByte = (uint8_t) dataPacket;
  dataPacket >>= 8;
  byte firstByte = (uint8_t) dataPacket;

  #if DEBUG
    Serial.print(F("Processing packet... Flags: "));
    Serial.println((bool) gameFlags);
    Serial.println(gameFlags);
  #endif
  if( gameFlags && (firstByte != 0x09)) {
    Serial.println("Got God Gun Packet");
    if(gameFlags & FLAG_CONFIGURATION_MODE) {
      processConfigData(firstByte, secondByte);
    } else if (gameFlags & FLAG_TEST_TARGET_MODE){
      reportReceivedByte(secondByte);
    } else if (gameFlags & FLAG_BORESIGHT_MODE) {
      fireShot();
    } else {
      return; //Can't do anything other than receive god gun commands when a game flag is set
    }
  } else if(firstByte >= 0x20 && firstByte <= 0xFF) {
    Serial.println("Got player shot packet");
    processPlayerWeaponHit(firstByte, secondByte);
    return;
  } else {
    Serial.println("Got system packet");
    switch(firstByte) {
      //0x00 is undefined
      
      case 0x01:
        addHealth(secondByte);
        break;

      case 0x02:
        addAmmo(secondByte);
        break;

      case 0x03:
        addClips(secondByte);
        break;

      case 0x04:
        addRpgRounds(secondByte);
        break;

      case 0x05:
        processNonPlayerWeaponHit(secondByte);
        break;

      case 0x06:
        disableWeapon(secondByte);
        break;

      case 0x07:
        accessCodeA(secondByte);
        break;

      case 0x08:
        accessCodeB(secondByte);
        break;

      case 0x09:
        godGunCommand(secondByte);
        break;

      case 0x0A:
        collateralDamage(secondByte);
        break;

      case 0x0B:
        gameSelect(secondByte);
        break;

      case 0x0C:
        weaponSelect(secondByte);
        break;

      //0x0D - 0x0E are undefined

      case 0x0F:
        sysData(secondByte);
        break;

      case 0x10:
      case 0x11:
      case 0x12:
      case 0x13:
      case 0x14:
        processScoreCommands(firstByte, secondByte);
        break;

      case 0x15:
        setConfigCode(secondByte);
        break;

      case 0x16:
        setTeamId(secondByte);
        break;

      case 0x17:
        setPlayerId(secondByte);
        break;

      //0x18 - 0x1F are undefined

     default:
        #if DEBUG
          Serial.println(F("Unsupported command"));
        #endif
        break;
    }
  }
}

void processPlayerWeaponHit(byte firstByte, byte secondByte) {
  byte teamId = firstByte >> 5;

  #if DEBUG
    Serial.print(F("Hit by "));
    getTeamName(teamId); //Put the team name in progmemString
    Serial.print(progmemString);
    Serial.print(F("("));
    Serial.print(teamId);
    Serial.print(F(") Player "));
    Serial.println(firstByte & 0b11111);
    Serial.print(F("Took "));
    Serial.print(secondByte);
    Serial.println(F(" Damage"));
  #endif

  takeDamage(secondByte);
}

/**
 * Take damage 
 */
void takeDamage(byte value) {
  if(value >= myHealth) {
    myHealth = 0;
    die();
  } else {
    myHealth -= value; 
  }
  printStatus();  
}

/**
 * Apply the ongoing damage from chemical weapons
 */
void applyChemicalBurn() {
  unsigned long secondsSinceStart = (millis() - chemicalBurnStart) / SECONDS_TO_MILLIS;
  uint8_t secondsToWait = chemicalBurnsApplied + 1;
  if(secondsSinceStart >= secondsToWait) {
    takeDamage(GS.ONGOING_CHEMICAL_DAMAGE);
    chemicalBurnsApplied++;
  }

  if(secondsSinceStart >= GS.CHEMICAL_BURN_TIME) {
    chemicalBurnStart = 0;
    chemicalBurnsApplied = 0;
  }
}

/**
 * Apply the ongoing damage from radiation weapons
 */
void applyRadiationPoison() {
  unsigned long secondsSinceStart = (millis() - radiationPoisonStart) / SECONDS_TO_MILLIS;
  uint8_t secondsToWait = radiationPoisonApplied + 1;
  if(secondsSinceStart >= secondsToWait) {
    takeDamage(GS.ONGOING_RADIATION_DAMAGE);
    radiationPoisonApplied++;
  }

  if(secondsSinceStart >= GS.RADIATION_POISON_TIME) {
    radiationPoisonStart = 0;
    radiationPoisonApplied = 0;
  }
}

/**
 * Check if a received data packet is valid
 * Per the Miles protocol, we're checking to see
 * if we got 17 bits and the parity bit matches.
 * Parity depends on game. Game 1 uses "odd" parity
 * and Game 2 uses "even" parity. The parity bit is
 * the 17th bit transmitted, for "even" parity, it will
 * be "1" if there are an odd number of "1" bits in the
 * data packet. So even parity checks for an even
 * number of "1"s in the packet.
 */
boolean validatePacket(uint32_t packetValue, byte numBits) {
  if(numBits != PACKET_TOTAL_BITS) {
    #if DEBUG
      Serial.println(F("Wrong number of bits"));
    #endif
    
    return false;
  }

  byte numOnes = countOnes(packetValue);
  boolean odd = numOnes % 2; //Will be 0 for even numbers
  if(PS.GAME_TYPE == GAME_A && odd) {
    //Game A uses odd parity
    #if DEBUG
      Serial.println(F("Got odd bits for GameA"));
    #endif
    
    return false;
  } else if (PS.GAME_TYPE == GAME_B && !odd) {
    //Game B uses even parity
    #if DEBUG
      Serial.println(F("Got odd bits for even parity game"));
    #endif
    return false;
  }

  return true;
}

void die() {
  gameFlags |= FLAG_DEAD;
  #if DEBUG
    Serial.println(gameFlags, BIN);
  #endif
  digitalWrite(LED, HIGH);
  for(byte cv = 0; cv < 3; cv++){
    playError();
    delay(250);
  }
}

/**
 * Send a shot packet over the IR LED
 * A shot packet consists of the following bytes:
 * tttppppp dddddddd
 * Where: 
 * ttt      is the Team ID
 * ppppp    is the Player ID
 * dddddddd is the damage done
 */
void fireShot() {
  irSender.send(MILESTAG, myShotPacket, 17);
}

/**
 * Detect if the player has pulled the trigger
 */
void senseFire() {
  if((bool) gameFlags) {
    //TODO: play error sound
    return;
  }
  
  int val = digitalRead(TRIGGER);
  if(val == LOW) {
    #if DEBUG
      Serial.println(F("Trigger pulled"));
    #endif

    if(! (buttons & BTN_TRIGGER)){ //If we're not already holding the trigger button down
      buttons |= BTN_TRIGGER;
      if(myAmmo > 0 || GS.CLIP_SIZE == 0){
        fireShot();
        myAmmo--;
        printStatus();
        if(! (gameFlags & FLAG_BORESIGHT_MODE)){
          playShot();
        }
      }
    }
  } else if(buttons & BTN_TRIGGER) {
    #if DEBUG
      Serial.println(F("Trigger released"));
    #endif
    //Un-set the trigger bit
    buttons &= (~BTN_TRIGGER); 
  }
}

/**
 * Detect if the player is reloading
 */
void senseReload() {
  if((bool) gameFlags) {
    //TODO: play error sound
    return;
  }
  
  int val = digitalRead(RELOAD);
  if(val == LOW) {
    if(! (buttons & BTN_RELOAD)){ //If we're not already holding the reload button down
      buttons |= BTN_RELOAD;
      if(myAmmo == 0 && (myClips > 0 || GS.MAX_CLIPS == 0)){ //We're out of ammo, and we still have clips
        if(GS.RELOAD_TIME > 0) {
          uint32_t reloadTime = millis() + (GS.RELOAD_TIME * TENTHS_TO_MILLIS);
          turnDisplayOn(DISPLAY_LONG);
          lcd.clear();
          lcd.print(F("Reloading..."));
          byte counter = 0;
          while(millis() < reloadTime) {
            if(counter <= 16){
              lcd.setCursor(counter % 16, 1);
              lcd.print(F("+"));
            } else {
              lcd.clear();
              lcd.print(F("Reloading..."));
              counter = 0;
            }
            delay(TENTHS_TO_MILLIS);
            counter++;            
          }
        }

        myClips--;
        myAmmo = GS.CLIP_SIZE;
        printStatus();
        playReload();
      }
    }
  } else if(buttons & BTN_RELOAD) {
    //Un-set the reload bit
    buttons &= (~BTN_RELOAD); 
  }
}

void processConfigData(byte &firstByte, byte &secondByte) {
  //TODO: Implement
}

//-------------------------System Message Handlers----------------------//

/**
 * Add a given amount to the current player's health, up to
 * the limit specified by MAX_HEALTH
 * 
 * @param health the amount of health to add
 */
void addHealth(byte &health){
  if(myHealth < GS.MAX_HEALTH) {
    incrementUpToMax(myHealth, health, GS.MAX_HEALTH);
  }

  //TODO: Play health up sound
}

/**
 * Add a given amount of ammunition to the current player's
 * ammo count, up to the limit specified by CLIP_SIZE. If
 * CLIP_SIZE = 0, then this has no effect as players will have
 * infinite ammo. 
 * 
 * If the amout to add is larger than the CLIP_SIZE
 * the the amount will be added to the number of player clips and
 * the remainder added to the ammo count. For example, if
 * CLIP_SIZE = 5 and the amount to add is 13, then 2 clips will
 * be added to the player inventory and 3 ammo to the current
 * ammo count. 
 * 
 * If the amount to add (or the remaining amount from above) is 
 * less than CLIP_SIZE and would increase the current ammo count
 * above CLIP_SIZE, the extra amount is discarded. There is no
 * current support for "partial" clips.
 * 
 * @param ammo the amount of ammo to add
 */
void addAmmo(byte &ammo){
  if(GS.CLIP_SIZE > 0) {
    byte ammoToAdd = ammo;
    if(ammo > GS.CLIP_SIZE) {
      byte numClips = ammo / GS.CLIP_SIZE;
      addClips(numClips);
      ammoToAdd = ammo % GS.CLIP_SIZE;
    }
    incrementUpToMax(myAmmo, ammoToAdd, GS.CLIP_SIZE);
  }
  //TODO: Play ammo add
}

/**
 * Add a given number of clips to the current player's clip
 * count, up to the limit specified by MAX_CLIPS. If 
 * MAX_CLIPS = 0, then this has no effect as players will
 * have infinite clips.
 * 
 * @param clips the number of clips to add
 */
void addClips(byte &clips) {
  if(GS.MAX_CLIPS > 0 && myClips < GS.MAX_CLIPS) {
    incrementUpToMax(myClips, clips, GS.MAX_CLIPS);
  }
  //TODO: Play ammo add
}

/**
 * Add a given number of RPG round to the current
 * player's count, up to the limit specified by MAX_RPGS.
 * If MAX_RPGS = 0, then this has no effect as players
 * will have infinite RPG rounds.
 * 
 * @param rpgs the number of RPG rounds to add
 */
void addRpgRounds(byte &rpgs) {
  if(GS.MAX_RPGS > 0 && myRPGs < GS.MAX_RPGS) {
    incrementUpToMax(myRPGs, rpgs, GS.MAX_RPGS);
  }
  //TODO: Play RPG add;
}

/**
 * Process a hit from a non player weapon
 */
void processNonPlayerWeaponHit(byte &packet) {
  byte damage = packet & 0b00011111;
  byte weaponId = packet >> 5;
  weaponId = weaponId & 0b111;

  #if DEBUG
    Serial.print(F("Hit by NPW "));
    getNonPlayerWeaponName(weaponId);
    Serial.print(progmemString);
    Serial.print(F(" ("));
    Serial.print(weaponId, BIN);
    Serial.print(F(") for Damage "));
    Serial.print(damage);
  #endif

  uint16_t totalDamage = damage;
  if(weaponId == 0x6) {
    takeDamage(totalDamage);
    chemicalBurnStart = millis();
  } else if (weaponId == 0x7) {
    takeDamage(totalDamage);
    radiationPoisonStart = millis();
  } else {
    #if DEBUG
      Serial.print(F("x2 ("));
      Serial.print(totalDamage);
      Serial.print(F(")"));
    #endif
    totalDamage = damage * 2;
    if(totalDamage > UINT8_MAX) {
      totalDamage = UINT8_MAX;
    }
    takeDamage(totalDamage);
  }

  #if DEBUG
    Serial.println();
  #endif
}

/**
 * Set the disabled bit in the game flags
 * and set the timeout value. If the number
 * of elapsed seconds since the player was last
 * disabled is less than DISABLE_WAIT, this has
 * no effect.
 * 
 * @param seconds the number of seconds to disable
 *    the player's weapon.
 */
void disableWeapon(byte &seconds) {
  unsigned long currentTime = millis();
  if(currentTime - disabledStart > (GS.DISABLED_WAIT * SECONDS_TO_MILLIS)) {
    gameFlags |= FLAG_DISABLED;
    if(seconds > GS.MAX_DISABLE_TIME) {
      disabledTimeout = GS.MAX_DISABLE_TIME;
    } else {
      disabledTimeout = seconds;
    }
    disabledStart = millis();
  }

  //TODO: Play disabled sound
}

void accessCodeA(byte &secondByte) {
  //TODO: Implement
}

void accessCodeB(byte &secondByte) {
  //TODO: Implement
}

/**
 * Process a god gun command
 * 
 * @param secondByte the command byte
 */
void godGunCommand(byte &secondByte) {
  #if DEBUG
    Serial.println("God Gun Command");
  #endif

  switch(secondByte) {
    case 0x00:
      adminKill();
      break;

    case 0x01:
      adminPause();
      break;

    case 0x02:
      respawnBase();
      break;

    case 0x03:
      startGame();
      break;

    case 0x04:
      respawnPlayer();
      break;

    case 0x05:
      initializePlayer();
      break;

    case 0x06:
      fullAmmo();
      break;

    case 0x07:
      gameOver();
      break;

    //0x08 is not defined

    case 0x09:
      configureGunMode();
      break;

    case 0x0A:
      configureSensorMode();
      break;

    case 0x0B:
      configureBaseMode();
      break;

    case 0x0C:
      configureMineMode();
      break;

    case 0x0D:
      resetMine();
      break;

    case 0x0E:
      testTargetMode();
      break;

    case 0x0F:
      boresightMode();
      break;

    case 0x10:
      awardPoint();
      break;

    case 0x11:
      awardMedal();
      break;

    case 0x12:
      awardPenalty();
      break;

    case 0x13:
      clearLog();
      break;

    case 0x14:
      clearScores();
      break;

    case 0x15:
      testSensor();
      break;

    //0x16 - 0xFF are undefined
    default:
      #if DEBUG
        Serial.print(F("Unrecognized god gun command "));
        Serial.println(secondByte, HEX);
      #endif
      break;
  }
}

/**
 * Apply collateral damage to the player
 */
void collateralDamage(byte &secondByte) {
  takeDamage(secondByte);
  //TODO: Play damage sound
}

/**
 * Select the game type (and thus play settings) for this game
 */
void gameSelect(byte &secondByte) {
  //TODO: Implement
}

void weaponSelect(byte &secondByte) {
  //TODO: Implement
}

void sysData(byte &secondByte) {
  //TODO: Implement
}

void processScoreCommands(byte firstByte, byte &secondByte) {
  //TODO: Implement
}

void setConfigCode(byte &secondByte) {
  //TODO: Implement
}

void setTeamId(byte &newId) {
  if(newId > 0 && newId < 8) {
    PS.TEAM_ID = newId;
    updateShotPacket();
    //TODO: Play success
  } else {
    //TODO: Play fail
  }
}

void setPlayerId(byte &newId) {
  if(newId >=0 && newId < 32) {
    PS.PLAYER_ID = newId;
    updateShotPacket();
    //TODO: Play success
  } else {
    //TODO: Play fail
  }
}


//---------------God Gun Functions-----------------//

/**
 * Administratively kill a player (or other device)
 */
void adminKill() {
  takeDamage(GS.MAX_HEALTH);
}

/**
 * Pause a player. A paused player can do nothing
 * except receive other "God Gun" commands. The disabled
 * timer will continue to run while paused.
 */
void adminPause() {
  gameFlags ^= FLAG_PAUSED;
  //TODO: Play pause sound
}

/**
 * Respawn this device if it's a base, otherwise has no effect
 */
void respawnBase() {
  if(PS.ROLE == ROLE_BASE) {
    myHealth = GS.DEFAULT_HEALTH;
    gameFlags &= (~FLAG_GAME_OVER); 
    //TODO: Play respawn sound
  }
}

/**
 * Start the game, set the game start time
 * and clear the game over flag
 */
void startGame() {
  gameStart = millis();
  gameFlags &= (~FLAG_GAME_OVER);
}

/**
 * Respawn this player, returning health to 
 * DEFAULT_HEALTH and clearing the game over flag
 */
void respawnPlayer() {
  if(PS.ROLE == ROLE_PLAYER) {
    myHealth = GS.DEFAULT_HEALTH;
    gameFlags &= (~FLAG_GAME_OVER);
    //TODO: Play respawn sound
  }
}

/**
 * Initialize this player to defaults
 * in preparation for a new game
 */
void initializePlayer() {
  myHealth = GS.DEFAULT_HEALTH;
  myClips = GS.DEFAULT_CLIPS;
  myAmmo = GS.CLIP_SIZE;
  myRPGs = GS.DEFAULT_RPGS;
  gameStart = 0;
  gameFlags = FLAG_GAME_OVER;
  //TODO: Play init sound
}

/**
 * Give the player a full ammo load out
 */
void fullAmmo() {
  myClips = GS.DEFAULT_CLIPS;
  myAmmo = GS.CLIP_SIZE;
  myRPGs = GS.DEFAULT_RPGS;
  //TODO: Play ammo add sound
}

/**
 * Set the player to game over status, set gameStart
 * to total game time
 */
void gameOver() {
  gameFlags |= FLAG_GAME_OVER;
  gameStart = millis() - gameStart;
}

/**
 * Put the gun into configuration mode and set the role to player
 */
void configureGunMode() {
  PS.ROLE = ROLE_PLAYER;
  gameFlags = FLAG_CONFIGURATION_MODE;
}

/**
 * Put the gun into sensor configuration mode.
 * At this time, just puts it into gun configure mode
 */
void configureSensorMode() {
  configureGunMode();
}

/**
 * Put the gun into configuration mode and set the role to base
 */
void configureBaseMode() {
  PS.ROLE = ROLE_BASE;
  gameFlags = FLAG_CONFIGURATION_MODE;
}

/**
 * Put the gun into configuration mode and set the role to mine
 */
void configureMineMode() {
  PS.ROLE = ROLE_MINE;
  gameFlags = FLAG_CONFIGURATION_MODE;
}

/**
 * Resets this mine device
 */
void resetMine() {
  if(PS.ROLE == ROLE_MINE) {
    //TODO: Implement
  }
}

/**
 * Set the gun into test target mode
 * In this mode, the gun will simply report
 * whether or not it receives valid data
 */
void testTargetMode() {
  gameFlags |= FLAG_TEST_TARGET_MODE;
}

/**
 * Set the gun into boresight mode
 * In this mode, the gun will repeatedly
 * fire shots without regard to ammo capacity
 */
void boresightMode() {
  gameFlags |= FLAG_BORESIGHT_MODE;
}

/**
 * Award a point to the player
 */
void awardPoint() {
  byte one = 1;
  incrementUpToMax(myScore, one, UINT8_MAX);
}

/**
 * Award a medal to the player
 */
void awardMedal() {
  byte one = 1;
  incrementUpToMax(myMedals, one, UINT8_MAX);
}

/**
 * Award a penalty to the player
 */
void awardPenalty() {
  byte one = 1;
  incrementUpToMax(myPenalties, one, UINT8_MAX);
}

void clearLog() {
  //TODO: Implement
}

void clearScores() {
  myScore = 0;
  myMedals = 0;
  myPenalties = 0;
}

void testSensor() {
  //TODO: Implement
}

void menuMode() {  
  menus.begin();
  updateShotPacket();
  printStatus();
}

//---------------Display Functions-----------------//
void printStatus() {
  lcd.clear();
  getTeamName(PS.TEAM_ID);
  lcd.print(progmemString);
  lcd.print(F(": "));
  lcd.print(PS.PLAYER_ID);
  lcd.setCursor(0,1);
  lcd.print(F("R"));

  char buffer[4];
  sprintf(buffer, "%03d", myAmmo);
  lcd.print(buffer);
  lcd.print(F(" C"));
  sprintf(buffer, "%03d", myClips);
  lcd.print(buffer);
  lcd.print(F(" H"));
  sprintf(buffer, "%03d", myHealth);
  lcd.print(buffer);
  turnDisplayOn(DISPLAY_SHORT); 
}

//---------------Utility Functions----------------//

