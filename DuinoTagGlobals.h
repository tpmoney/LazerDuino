/**
 * DuinoTagGlobals.h
 *
 * Global variables that parts of the Duino Tag system need to access
 */

#ifndef GLOBALS_H
#define GLOBALS_H

#include "Arduino.h"
#include "pitches.h"
class MenuManager;

//Display
#include <LiquidCrystal.h>
#include <Wire.h>

//IRLib2
#include <IRLibRecvPCI.h>
#include <IRLibDecodeBase.h>
#include <IRLibSendBase.h>
#include <IRLib_P13_MilesTag.h>
#include <IRLibCombo.h>

//Audio
#include <DFRobotDFPlayerMini.h>
#include <SoftwareSerial.h>

//----------------Miles Tag Protocol Settings----------------//
//YOU SHOULD NOT CHANGE THESE UNLESS YOUR GOING TO  MODIFY THE MILES TAG PROTOCOL FOR YOUR SYSTEM
// CHANGING THESE VALUES TO BE OUTSIDE THE DEFAULT RANGE MAY REQUIRE TAILORING SOME OF THE ASSOCIATED
// CODE IN THIS PROJECT.
// Note that some of the maximums defined here are aboslute maxes, mostly used for menu limts that can 
// be further limited in game by game specific settings
const byte PACKET_TOTAL_BITS      = 17;   //A data packet is 2 bytes and a parity bit
const byte MIN_PLAYER_ID          = 0;    //Minimum allowed player id
const byte MAX_PLAYER_ID          = 31;   //Maximum allowed player id
const byte MIN_TEAM_ID            = 1;    //Minimum allowed team id
const byte MAX_TEAM_ID            = 7;    //Maximum allowed team id
const byte MIN_PLAYER_POWER       = 1;    //Minimum allowed player power (damage)
const byte MAX_PLAYER_POWER       = 100;  //Maximum allowed player power
const byte MIN_ROLE_ID            = 0;    //Minimum allowed role ID
const byte MAX_ROLE_ID            = 0;    //2;    //Maximum allowed role ID (currently 0 because we don't support any other roles)
const byte MIN_GAME_TYPE          = 0;    //Minimum allowed game type
const byte MAX_GAME_TYPE          = 1;    //Maximum allowed game type
const byte MIN_GAME_HEALTH        = 1;    //Minimum allowed player health
const byte MAX_GAME_HEALTH        = 255;  //Maximum allowed player health
const byte MIN_CLIP_QTY           = 0;    //Minimum allowed player clips quanty, a value of 0 represents infinite
const byte MAX_CLIP_QTY           = 255;  //Maximum allowed player clips quanity
const byte MIN_CLIP_SIZE          = 0;    //Minimum allowed clip size, a value of 0 representes infinite
const byte MAX_CLIP_SIZE          = 255;  //Maximum allowed clip size
const byte MIN_GAME_RPG           = 0;    //Minimum allowed RPGs
const byte MAX_GAME_RPG           = 255;  //Maximum allowed RPGs
const byte MIN_DISABLE_TIMEOUT    = 0;    //Minimum allowed time for a disable command to have effect
const byte MAX_DISABLE_TIMEOUT    = 255;  //Maximum allowed time for a disable command to have effect
const byte MIN_ONGOING_CHEM_BURN  = 0;    //Minimum allowed ongoing damage from chemical burns
const byte MAX_ONGOING_CHEM_BURN  = 255;  //Maximum allowed ongoing damage from chemical burns
const byte MIN_CHEM_BURN_TIME     = 0;    //Minimum allowed chemical burn time (in seconds)
const byte MAX_CHEM_BURN_TIME     = 255;  //Maximum allowed chemical burn time (in seconds)
const byte MIN_ONGOING_RAD_POISON = 0;    //Minimum allowed ongoing damage from radiation poison
const byte MAX_ONGOING_RAD_POISON = 255;  //Maximum allowed ongoing damage from radiation poison
const byte MIN_RAD_POISON_TIME    = 0;    //Minimum allowed radiation poison time (in seconds)
const byte MAX_RAD_POISON_TIME    = 255;  //Maximum allowed radiation poison time (in seconds)
const byte MIN_RELOAD_TIME        = 0;    //Minimum allowed reload time (in 0.1 second increments)
const byte MAX_RELOAD_TIME        = 50;   //Maximum allowed reload time (in 0.1 second increments)


//-----------------Sound Effects-----------------------------//
//Sound Effects
const byte SHOT_SOUND     = 1;
const byte RELOAD_SOUND   = 2;
const byte ERROR_SOUND    = 3;
const byte HIT_SOUND      = 4;
const byte SUCCESS_SOUND  = 5;

//--------------------Constants------------------------------//

//Pins
const byte SENSOR             = 2;  //IR Sensor on Pin 2 required by IRLib2 settings
const byte IR_LED             = 3;  //IR LED on pin 3 required by IRLib2 settings
const byte TRIGGER            = 4;  //Trigger button on pin 4
const byte RELOAD             = 5;  //Reload button on pin 5
const byte LCD_BACKLIGHT      = 6;  //Pin to control backlight of LCD
const byte FUNCTION           = 7;  //Function button on pin 7
const byte AUDIO_RX           = 10; //RX pin for audio player
const byte AUDIO_TX           = 11; //TX pin for audio player
const byte LED                = 13; //Output LED on pin 13

const byte LCD_RS         = A0;
const byte LCD_EN         = A1;
const byte LCD_DB4        = A2;
const byte LCD_DB5        = A3;
const byte LCD_DB6        = A4;
const byte LCD_DB7        = A5;


//Game Status Flag Bits
const byte FLAG_DISABLED            = 1;   //Flag that indicates the player's gun is disabled
const byte FLAG_DEAD                = 2;   //Flag that indicates the player is dead
const byte FLAG_PAUSED              = 4;   //Flag that indicates the player is paused
const byte FLAG_GAME_OVER           = 8;   //Flag that indicates the game is over
const byte FLAG_CONFIGURATION_MODE  = 16;  //Flag that inidcates the system is in configuration mode 
const byte FLAG_TEST_TARGET_MODE    = 32;  //Flag that indicates the system is in "test target" mode
const byte FLAG_BORESIGHT_MODE      = 64;  //Flag that indicates the system is in "boresight" mode
const byte FLAG_MENU_MODE           = 128; //Flag that indicates the system is in menu mode

//Button bits
const byte BTN_TRIGGER          = 1;    //Flag that indicates the trigger button is held down
const byte BTN_RELOAD           = 2;    //Flag that indicates the reload button is held down
const byte BTN_FUNCTION         = 4;    //Flag that indicates the function button is held down
const byte BTN_LONG_PRESS       = 128;  //Flag that indicates the button was held for LONG_PRESS seconds

//Button handling values
const uint16_t SECONDS_TO_MILLIS  = 1000; //Number of milliseconds in a second
const uint16_t TENTHS_TO_MILLIS   = 100;  //Number of milliseconds in a tenth of a second
const byte DEBOUNCE_TIME          = 20;   //How long to wait before allowing a read of a button release
const byte LONG_PRESS             = 1;    //The number of seconds a button has to be held for it to count as a long press
const byte TRIGGER_GO_BACK_TOTAL  = 6;    //The total number of seconds the trigger must be held down to trigger an exit from the menus

//Times (in seconds) to leave the LCD display on
const byte DISPLAY_INFINITE     = 0;
const byte DISPLAY_SHORT        = 3;
const byte DISPLAY_LONG         = 10;

//Store team names in program memory
const PROGMEM char team_000[]     = "Sys";
const PROGMEM char team_001[]     = "Red";
const PROGMEM char team_010[]     = "Blue";
const PROGMEM char team_011[]     = "Yellow";
const PROGMEM char team_100[]     = "Green";
const PROGMEM char team_101[]     = "Black";
const PROGMEM char team_110[]     = "White";
const PROGMEM char team_111[]     = "Purple";
const PROGMEM char* const TEAMS[]   = {team_000, team_001, team_010, team_011, team_100, team_101, team_110, team_111};

//Store non-player weapon names in program memory
const PROGMEM char npw_000[]    = "Claymore";
const PROGMEM char npw_001[]    = "Grenade";
const PROGMEM char npw_010[]    = "50 Cal.";
const PROGMEM char npw_011[]    = "Rocket";
const PROGMEM char npw_100[]    = "Mortar";
const PROGMEM char npw_101[]    = "Sentry";
const PROGMEM char npw_110[]    = "Chemical";
const PROGMEM char npw_111[]    = "Radiation";
const PROGMEM char* const NON_PLAYER_WEAPONS[] = {npw_000, npw_001, npw_010, npw_011, npw_100, npw_101, npw_110, npw_111};

//Store roles in program memory
const PROGMEM char role_00[]    = "Player";
const PROGMEM char role_01[]    = "Base";
const PROGMEM char role_10[]    = "Mine";
const PROGMEM char* const ROLES[] = {role_00, role_01, role_10};

//Role IDs for progmatic use
const byte ROLE_PLAYER  = 0;
const byte ROLE_BASE    = 1;
const byte ROLE_MINE    = 2;

//Store game types in program memory
const PROGMEM char game_a[]   = "Game A";
const PROGMEM char game_b[]   = "Game B";
const PROGMEM char* const GAME_TYPES[] = {game_a, game_b};

//Game types for progmatic use
const byte GAME_A = 0;
const byte GAME_B = 1;

//-------------------------Variables (externs init in Duino_Tag.ino)------------------------------//
// Reserve max progmem string size in memory for processing
// Currently 15 characters, plus the null terminator
extern char progmemString[];

//Timing values
extern unsigned long gameStart;             //Time when the game started, or the total game time when in GAME OVER
extern unsigned long disabledStart;         //Time when the player was last disabled
extern unsigned long displayTimeout;      //Time point when the display will turn off next
extern unsigned long buttonTimer;           //Used for determining how long a button has been held for
extern byte disabledTimeout;              //Seconds until the disabled status is removed
extern unsigned long chemicalBurnStart;   //Time when the player last received chemical damage
extern byte chemicalBurnsApplied;           //When receiving chemical damage, incremented to count the number of times damage is applied
extern unsigned long radiationPoisonStart; //Time when the player last received radiation damage
extern byte radiationPoisonApplied;         //When receiving radiation damage, incremented to count the number of times damage is applied

//IR Processing
extern bool readData;                     //Whether or not we read valid data from the IR sensor
extern byte numBits;                      //The number of bits received
extern uint32_t irValue;                  //The value of the IR message

//Flag vars
extern byte gameFlags;                    //Game flags for various statuses
extern byte buttons;                      //Bits are set to 1 when a button is being held

//Objects
extern LiquidCrystal lcd;
extern IRdecode irDecoder;
extern IRsend irSender;
extern IRrecvPCI irRemote;
extern MenuManager menus;
extern DFRobotDFPlayerMini audioPlayer;
extern SoftwareSerial softSerial;



#endif //GLOBALS_H