/**
 * GameParameters.h
 *
 * Parameters that define how the Duino Tag game is played
 */

#ifndef GAME_PARAM_H
#define GAME_PARAM_H

#include "Arduino.h"

const char SETTINGS_VERSION[4] = "DT1"; //The version of the game and player settings. If this differs, we will not load settings from EEPROM that have been saved previously

//End User Configurable Parameters (Declared and initial values set in Duino_Tag.ino)
struct GameSettings {
  byte MAX_HEALTH;                //Maximum Player Health
  byte DEFAULT_HEALTH;            //Starting Player Health (also when revived)
  byte MAX_CLIPS;                 //The maximum number of clips of ammunition a player can have (set to 0 for infinite clips)
  byte DEFAULT_CLIPS;             //Starting player ammo clips (in addition to starting with CLIP_SIZE ammo)
  byte CLIP_SIZE;                 //The maximum amount of ammo a single clip will hold (set to 0 for infinite ammo without reloads)
  byte MAX_RPGS;                  //The maximum amount of RPGs a player can have (set to 0 for infinite RPGs)
  byte DEFAULT_RPGS;              //Starting player RPGs
  byte MAX_DISABLE_TIME;          //The max number of seconds a disable command can set (note this is not the total max disable time, see wait below)
  byte DISABLED_WAIT;             //The number of seconds that must have elapsed since the player was last disabled before another command will take effect
                                  //  By setting this to less than the max disable time, it's possible for a player to have a longer effective disable time
                                  //  than allowed by the MAX_DISABLE_TIME setting, up to 255 seconds which is the limit of the byte used to store the current
                                  //  disable timeout. 
  byte ONGOING_CHEMICAL_DAMAGE;   //The amount of damage per second chemical weapons apply to the player, setting this too high will make
                                  //  chemical damage effectively instantly lethal. Recommend setting to no more than
                                  //  MAX_HEALTH / CHEMICAL_BUN_TIME / 2
  byte CHEMICAL_BURN_TIME;        //The max number of seconds that a chemical weapon will continue apply damage to the player
  byte ONGOING_RADIATION_DAMAGE;  //The amount of damage per second radiation weapons apply to the player, setting this too high will make
                                  //  radiation damage effectively instantly lethal. Recommend setting to no more than
                                  //  MAX_HEALTH / RADIATION_POISON_TIME / 2
  byte RADIATION_POISON_TIME;     //The max number of seconds that a radiation weapon will continue to apply damage to the player
  byte RELOAD_TIME;               //The number of seconds it takes to re-load a weapon, 0 is instant.
};

//Configurable player settings (Declared and initial values set in Duino_Tag.ino)
struct PlayerSettings {
  byte PLAYER_ID;                 //Invidiual player ID
  byte TEAM_ID;                   //Player's Team ID
  byte POWER;                     //Player's power level
  byte ROLE;                      //This device's role
  byte GAME_TYPE;                 //Which game we're playing in (for parity purposes)
};

extern GameSettings GS;
extern PlayerSettings PS;


//Current Game parameters (Declared in Duino_Tag.ino)
extern byte myHealth;                   //Current player health level
extern byte myClips;                    //Current clip inventory for player
extern byte myAmmo;                     //Current ammo count in the current clip
extern byte myRPGs;                     //Current RPG ammo count
extern byte myScore;                    //Current score count
extern byte myMedals;                   //Current medals count
extern byte myPenalties;                //Current penalties count

// Caclulated at startup or any time team or player ID changes
// Saves from having to recalculate each shot
extern uint32_t myShotPacket;

#endif //GAME_PARAM_H