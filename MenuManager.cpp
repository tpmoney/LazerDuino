/**
 * MenuManager.cpp
 *
 * Manages the menu system of the Duino Tag
 */

#include "MenuManager.h"
#include "Utilities.h"
#include "DuinoTagGlobals.h"
#include "GameParameters.h"


/**
 * Get a menu instruction out of program memory and
 * copy it to progmemString
 */
void getMenuInstruction(byte instruction) {
  strcpy_P(progmemString, (char *)pgm_read_word(&(MENU_INSTRUCTIONS[instruction])));
}

/**
 * A delay function for the menu system that breaks a delay into
 * 100ms blocks in order to be responsive to user button presses
 */
boolean menuDelay(int delayTime) {
  while(delayTime > 0) {
    if(digitalRead(FUNCTION) == LOW){
      buttons = BTN_FUNCTION;
      buttonTimer = millis();
      return true;
    } else if (digitalRead(TRIGGER) == LOW){
      buttons = (BTN_TRIGGER + BTN_LONG_PRESS); //Fake a long press here
      buttonTimer = millis();
      return true;
    }
    delay(100);
    delayTime -= 100;
  }
  return false;
}

/**
 * Check if a given button has performed a complete press and release or long press
 * @param the button to check
 */
bool checkCompletePress(byte buttonToCheck, byte buttonFlag) {
  bool pressed = digitalRead(buttonToCheck) == LOW;
  if(buttons & buttonFlag) { //The button was previously being held
    if(pressed) { //And it's still being pressed
      byte holdTime = (millis() - buttonTimer) / SECONDS_TO_MILLIS;
      if(holdTime >= LONG_PRESS) { //For longer than long press
        buttons |= (BTN_LONG_PRESS + buttonFlag); //Set the long press flag
        return true; //Return true to indicate a successful press
      } else {
        return false; //Need to keep checking until we've passed the LONG_PRESS time
      } 

    } else { //And it no longer is
      if(millis() - buttonTimer > DEBOUNCE_TIME) { //Passed the debounce timeout
        buttonTimer = 0;
        return true; //Indicate the button was pressed and released
      } else {
        return false; //Need to keep checking until we've passed the DEBOUNCE_TIME
      }
    }

  } else if(pressed) { //The button is newly pressed
    buttons |= buttonFlag;
    buttonTimer = millis();
    return false; //Now we need to wait for a release or a long press 
  } else {
    //The button was neither newly pressed, held down nor released
    return false;
  }
}

/**
 * Wait until the user presses a button and process that press
 * setting the button bit and the long press bit in "buttons" if 
 * applicable. Polls the buttons every 10ms
 * @return the button that was pressed
 */
byte waitPress() {
  boolean continuedLongPress = (buttons & BTN_LONG_PRESS); //If we came into this function, and the button was being long pressed already
  buttons = 0; //Reset button state

  if(! continuedLongPress){ //If we didn't come in already holding the button for a long press
    while(digitalRead(FUNCTION) == LOW || digitalRead(RELOAD) == LOW || digitalRead(TRIGGER) == LOW) {
      delay(250); //Wait until buttons have been released before beginning loop
    }
  }

  //Wait for a press and release or a long press, or the release of the current long press
  while(!checkCompletePress(FUNCTION, BTN_FUNCTION) 
      && !checkCompletePress(RELOAD, BTN_RELOAD) 
      && !checkCompletePress(TRIGGER, BTN_TRIGGER)) { //While none of the buttons have been completely pressed

    delay(10); //Poll the buttons every 10ms
  }

  if(continuedLongPress && !(buttons & BTN_LONG_PRESS)) {
    //This means a release of an existing long press happened, 
    // we actually want to wait for the next button press to get
    // the user's real intent
    return waitPress();
  } else if(buttons & BTN_FUNCTION) {
    return BTN_FUNCTION;
  } else if (buttons & BTN_RELOAD) {
    return BTN_RELOAD;
  } else {
    return BTN_TRIGGER;
  }
}

boolean triggerGoBack() {
  if(gameFlags & FLAG_MENU_MODE) {
    if( (buttons & (BTN_TRIGGER + BTN_LONG_PRESS)) == (BTN_TRIGGER + BTN_LONG_PRESS) ) {
      turnDisplayOn(DISPLAY_INFINITE);
      //Delay for a bit longer
      byte seconds = (millis() - buttonTimer) / SECONDS_TO_MILLIS;
      for(/*seconds */; 
          digitalRead(TRIGGER) == LOW && seconds < TRIGGER_GO_BACK_TOTAL; 
          seconds = (millis() - buttonTimer) / SECONDS_TO_MILLIS) {
        lcd.clear();
        lcd.print(F("Hold for: "));
        lcd.print(TRIGGER_GO_BACK_TOTAL - seconds);
        lcd.setCursor(0,1);
        lcd.print(F("To Exit Config"));
        delay(500);
      }
      if(digitalRead(TRIGGER == LOW) && seconds >= TRIGGER_GO_BACK_TOTAL) {
        buttons = 0;
        gameFlags = 0;
        lcd.clear();
        lcd.print(F("Going Home..."));
        while(digitalRead(TRIGGER) == LOW) {
          delay(100); //Wait for the user to release the button
        }
        return true; //We held the trigger long enough, go back
      } else {
        return false; //The user let go of the trigger
      }
    } else {
      return false; //The trigger button was not held down for a long press
    }
  } else {
    return true; //We're no longer in menu mode, so let's go back
  }
}

/**
 * Loop over a provided array of menu strings and return the
 * selected menu item.
 * @param menuArray an array of strings, consisting of to entries (index and index+1)
 *    of strings to display for each menu item
 * @param arraySize the total size of the provided array
 * @param lastIndex the last index that was selected, incase we want to start from
 *    the middle of the array somewhere
 * @return the index of the selected menu item
 */
byte loopMenu(const char* const menuArray[], byte arraySize, byte lastIndex =0) {
  delay(50); //Short delay to prevent accidental bounce selection
  for(int cv = lastIndex; cv < arraySize; cv+=2) {
    //Print the menu values to screen
    lcd.clear();
    strcpy_P(progmemString, (char *)pgm_read_word(&(menuArray[cv])));
    lcd.print(progmemString);
    lcd.setCursor(0,1);
    strcpy_P(progmemString, (char *)pgm_read_word(&(menuArray[cv + 1])));
    lcd.print(progmemString);
    
    //Wait for user input
    byte press = waitPress();
    switch(press) {
      case BTN_FUNCTION:
        if(cv == arraySize -2){
          cv = -2; //Reset to the beginning of the menus
        }
        continue;

      case BTN_RELOAD:
        return cv;

      case BTN_TRIGGER:
        return GO_BACK_ONE; //Caller is responsible for checking if this was a long press
    }
  }
  return GO_BACK_ONE;
}

/**
 * Display numbers for selection. Requires a minimum of digits + 1 character positions
 * @param min the minimum value allowed
 * @param max the maximum value allowed
 * @param digits the number of digits to display for the number (1-4)
 * @param holdIncrement the amount to increase when the function button is held
 * @param rowPos the row position on the display to write from
 * @param colPos the column position on the display to write from
 * @param setting the setting to compare to when marking the current value
 * @param textVals an optional aray of strings to display instead of the numeric value if 
 *    provided, then rather than needing a minimum of digits + 1, we need a minimum of the
 *    max string size + 1, which realistically limits us to 15 characters
 * @return true if the setting was saved
 */
bool loopNumbers(byte min, byte max, byte digits, byte holdIncrement, 
    byte rowPos, byte colPos, byte &setting, const char* const textVals[] = 0, bool zeroInfinite = false) {
  delay(50); //Short delay to prevent accidental bounce selection
  byte cv = setting <= max ? setting : min; //Incase for whatever reason a current setting is greater than the max
  for(/*cv*/; cv<=max; cv++) {
    lcd.setCursor(colPos,rowPos);
    if(textVals == 0) { //Display the raw numeric values
      if(cv == 0 && zeroInfinite) {
        lcd.print((char) 0xF3);
        for(byte cv2 = 0; cv2 < digits; cv2++) {
          lcd.print(menu_empty[0]);
        }
      } else {
        char buffer[digits + 1];
        switch (digits) {
          case 1:
            sprintf(buffer, "%01d", cv);
            break;
          case 2:
            sprintf(buffer, "%02d", cv);
            break;
          case 3:
            sprintf(buffer, "%03d", cv);
            break;
          case 4:
            sprintf(buffer, "%04d", cv);
            break;
        }
        lcd.print(buffer);
      }
    } else { //Look up the numeric values in the provided text array
      strcpy_P(progmemString, (char *)pgm_read_word(&(textVals[cv])));
      lcd.print(progmemString);
    }
    if(cv == setting){
      lcd.print(F("*"));
    } else {
      lcd.print(F(" "));
    }

    byte press = waitPress();
    switch (press) {
      case BTN_FUNCTION: 
        if(buttons & BTN_LONG_PRESS){
          cv += holdIncrement -1;
        }
        if(cv >= max){
          cv = min -1;
        }
        continue;

      case BTN_RELOAD:
        setting = cv;
        return true;

      case BTN_TRIGGER:
        return false;
    }
  }
  return false; //Should never get here
}

/**
 * Allow the user to select the current player id
 */
void beginPlayerIdSelection() {
  lcd.clear();
  lcd.print(F("Set Player Id:"));
  lcd.setCursor(0,1);
  lcd.print(F("PID: "));
  loopNumbers(MIN_PLAYER_ID, MAX_PLAYER_ID, 2, 10, 1, 5, PS.PLAYER_ID);
}

/**
 * Allow the user to select the current player team
 */
void beginTeamIdSelection() {
  lcd.clear();
  lcd.print(F("Set Team:"));
  loopNumbers(MIN_TEAM_ID, MAX_TEAM_ID, 0, 1, 1, 0, PS.TEAM_ID, TEAMS);
}

/**
 * Allow the player to select the current player's power level
 */
void beginPowerLevelSelection() {
  lcd.clear();
  lcd.print(F("Set Power:"));
  lcd.setCursor(0,1);
  lcd.print(F("DMG: "));
  loopNumbers(MIN_PLAYER_POWER, MAX_PLAYER_POWER, 3, 10, 1, 5, PS.POWER);
}

/**
 * Allow the player to select the current player's role
 */
void beginRoleSelection() {
  lcd.clear();
  lcd.print(F("Set Role:"));
  loopNumbers(MIN_ROLE_ID, MAX_ROLE_ID, 0, 1, 1, 0, PS.ROLE, ROLES);
}

/**
 * Allow the player to select the current player's game
 */
void beginGameVersionSelection() {
  lcd.clear();
  lcd.print(F("Set Parity:"));
  lcd.setCursor(0,1);
  loopNumbers(MIN_GAME_TYPE, MAX_GAME_TYPE, 0, 1, 1, 0, PS.GAME_TYPE, GAME_TYPES);
}

/**
 * Display the playser settings menu
 */
void beginPlayerSettingsMenu() {
  byte selected = 0;
  while(true) { 
    buttons = 0;
    buttonTimer = 0;
    selected = loopMenu(PLAYER_MENU, 10, selected);
    if(triggerGoBack()) {
      return;
    }
    switch(selected) {
      case PM_PLAYER_ID:
        beginPlayerIdSelection();
        break;

      case PM_TEAM_ID:
        beginTeamIdSelection();
        break;

      case PM_POWER_LEVEL:
        beginPowerLevelSelection();
        break;

      case PM_ROLE:
        beginRoleSelection();
        break;

      case PM_GAME_VERSION:
        beginGameVersionSelection();
        break;

      case GO_BACK_ONE:
        return;
    }
  }
}

/**
 * Allow the maximum player health for a game to be set
 */
void beginMaxHealthSelection() {
  lcd.clear();
  lcd.print(F("Set Max Health:"));
  if(loopNumbers(MIN_GAME_HEALTH, MAX_GAME_HEALTH, 3, 20, 1, 0, GS.MAX_HEALTH)) {
    if (GS.DEFAULT_HEALTH > GS.MAX_HEALTH) {
      GS.DEFAULT_HEALTH = GS.MAX_HEALTH;
    }
    if (myHealth > GS.MAX_HEALTH) {
      myHealth = GS.MAX_HEALTH;
    }
  }
}


/**
 * Allow the default player health for a game to be set
 */
void beginDefaultHealthSelection() {
  lcd.clear();
  lcd.print(F("Set Def. Health:"));
  loopNumbers(MIN_GAME_HEALTH, GS.MAX_HEALTH, 3, 20, 1, 0, GS.DEFAULT_HEALTH);
}

/**
 * Allow the maximum clips for a player to be set, setting this
 * to 0 will allow the player to have an infinite clip qty
 */
void beginMaxClipsSelection() {
  lcd.clear();
  lcd.print(F("Set Max Clip"));
  lcd.setCursor(0,1);
  lcd.print(F("Qty: "));
  if(loopNumbers(MIN_CLIP_QTY, MAX_CLIP_QTY, 3, 20, 1, 5, GS.MAX_CLIPS, 0, true) && GS.MAX_CLIPS > 0) {
    if (GS.DEFAULT_CLIPS > GS.MAX_CLIPS) {
      GS.DEFAULT_CLIPS = GS.MAX_CLIPS;
    }
    if (myClips > GS.MAX_CLIPS) {
      myClips = GS.MAX_CLIPS;
    }
  }
}

/**
 * Allow the default number of clips to be set
 */
void beginDefaultClipsSelection() {
  lcd.clear();
  lcd.print(F("Set Default Clip"));
  lcd.setCursor(0,1);
  lcd.print(F("Qty: "));
  loopNumbers(MIN_CLIP_QTY, GS.MAX_CLIPS, 3, 20, 1, 5, GS.DEFAULT_CLIPS);
}

/**
 * Allow the clip size to be set. Setting this to 
 * 0 will allow the player to fire continuously
 * without reloading
 */
void beginClipSizeSelection() {
  lcd.clear();
  lcd.print(F("Set Clip Size: "));
  if(loopNumbers(MIN_CLIP_SIZE, MAX_CLIP_SIZE, 3, 20, 1, 0, GS.CLIP_SIZE, 0, true) && GS.CLIP_SIZE > 0) {
    if(myAmmo > GS.CLIP_SIZE) {
      myAmmo = GS.CLIP_SIZE;
    }
  }  
}

/**
 * Allow the max number of RPGs to be set. Setting this
 * to 0 will allow the player to fire an unlimited number
 * of RPGs
 */
void beginMaxRPGSelection() {
  lcd.clear();
  lcd.print(F("Set Max RPG Qty:"));
  if(loopNumbers(MIN_GAME_RPG, MAX_GAME_RPG, 3, 20, 1, 0, GS.MAX_RPGS, 0, true) && GS.MAX_RPGS > 0) {
    if(myRPGs > GS.MAX_RPGS) {
      myRPGs = GS.MAX_RPGS;
    }
  }
}

/**
 * Allow the default number of RPGs a player starts with
 * to be set
 */
void beginDefaultRPGSelection() {
  lcd.clear();
  lcd.print(F("Set Default RPG"));
  lcd.setCursor(0,1);
  lcd.print(F("Qty: "));
  loopNumbers(MIN_GAME_RPG, GS.MAX_RPGS, 3, 20, 1, 5, GS.DEFAULT_RPGS);
}

/**
 * Allow the max time to the disable command to
 * have an effect to be set
 */
void beginMaxDisableTimeSelection() {
  lcd.clear();
  lcd.print(F("Set Max Disable"));
  lcd.setCursor(0,1);
  lcd.print(F("Cmd. Sec.: "));
  loopNumbers(MIN_DISABLE_TIMEOUT, MAX_DISABLE_TIMEOUT, 3, 20, 1, 11, GS.MAX_DISABLE_TIME);
}

/**
 * Allow the disable wait time to be set
 */
void beginDisableWaitTimeSelection() {
  lcd.clear();
  lcd.print(F("Set Disable Wait"));
  lcd.setCursor(0,1);
  lcd.print(F("Cmd. Sec.: "));
  loopNumbers(MIN_DISABLE_TIMEOUT, MAX_DISABLE_TIMEOUT, 3, 20, 1, 11, GS.DISABLED_WAIT);
}

/**
 * Allow the ongoing chemical burn damage to be set
 */
void beginOngoingChemicalBurnDamageSelection() {
  lcd.clear();
  lcd.print(F("Set Chem. Burn"));
  lcd.setCursor(0,1);
  lcd.print(F("Damage: "));
  loopNumbers(MIN_ONGOING_CHEM_BURN, MAX_ONGOING_CHEM_BURN, 3, 20, 1, 8, GS.ONGOING_CHEMICAL_DAMAGE);
}

/**
 * Allow setting the chemical burn time
 */
void beginChemicalBurnTimeSelection() {
  lcd.clear();
  lcd.print(F("Set Chem. Burn"));
  lcd.setCursor(0,1);
  lcd.print(F("Seconds: "));
  loopNumbers(MIN_CHEM_BURN_TIME, MAX_CHEM_BURN_TIME, 3, 20, 1, 9, GS.CHEMICAL_BURN_TIME);
}

/**
 * Allow the ongoing radiation poison damage to be set
 */
void beginOngoingRaditionPoisonDamageSelection() {
  lcd.clear();
  lcd.print(F("Set Rad. Poison"));
  lcd.setCursor(0,1);
  lcd.print(F("Damage: "));
  loopNumbers(MIN_ONGOING_RAD_POISON, MAX_ONGOING_RAD_POISON, 3, 20, 1, 8, GS.ONGOING_RADIATION_DAMAGE);
}

/**
 * Allow the radiation poison time to be set
 */
void beginRadiationPoisonTimeSelection() {
  lcd.clear();
  lcd.print(F("Set Rad. Poison"));
  lcd.setCursor(0,1);
  lcd.print(F("Seconds: "));
  loopNumbers(MIN_RAD_POISON_TIME, MAX_RAD_POISON_TIME, 3, 20, 1, 8, GS.RADIATION_POISON_TIME);
}


void beginReloadTimeSelection() {
  lcd.clear();
  lcd.print(F("Set Reload Time"));
  lcd.setCursor(0,1);
  lcd.print(F("(1/10 Sec): "));
  loopNumbers(MIN_RELOAD_TIME, MAX_RELOAD_TIME, 2, 5, 1, 12, GS.RELOAD_TIME);
}


/**
 * Display the game settings menu
 */
void beginGameSettingsMenu() {
  byte selected = 0;
  while(true) { 
    buttons = 0;
    buttonTimer = 0;
    selected = loopMenu(GAME_MENU, 28, selected);
    if(triggerGoBack()) {
      return;
    }
    switch(selected) {
      case GM_MAX_HEALTH:
        beginMaxHealthSelection();
        break;

      case GM_DEF_HEALTH:
        beginDefaultHealthSelection();
        break;

      case GM_MAX_CLIPS:
        beginMaxClipsSelection();
        break;

      case GM_DEF_CLIPS:
        beginDefaultClipsSelection();
        break;

      case GM_CLIP_SIZE:
        beginClipSizeSelection();
        break;

      case GM_MAX_RPG:
        beginMaxRPGSelection();
        break;

      case GM_DEF_RPG:
        beginDefaultRPGSelection();
        break;

      case GM_MAX_DISABLE:
        beginMaxDisableTimeSelection();
        break;

      case GM_DISABLE_WAIT:
        beginDisableWaitTimeSelection();
        break;

      case GM_ONGOING_CHEM:
        beginOngoingChemicalBurnDamageSelection();
        break;

      case GM_CHEM_TIME:
        beginChemicalBurnTimeSelection();
        break;

      case GM_ONGOING_RAD:
        beginOngoingRaditionPoisonDamageSelection();
        break;

      case GM_RAD_TIME:
        beginRadiationPoisonTimeSelection();
        break;

      case GM_RELOAD_TIME:
        beginReloadTimeSelection();
        break;

      case GO_BACK_ONE:
        return;
    }
  }
}

/**
 * Save the player settings to EEPROM
 */
void savePlayerSettings() {
  lcd.clear();
  lcd.print(F("Saving Player"));
  lcd.setCursor(0,1);
  lcd.print(F("Settings..."));
  savePlayerSettingsToEEPROM();
  delay(1000);
  lcd.clear();
  lcd.print(F("Saved!"));
  delay(1000);
}

/**
 * Save the game settings to EEPROM
 */
void saveGameSettings() {
  lcd.clear();
  lcd.print(F("Saving Game"));
  lcd.setCursor(0,1);
  lcd.print(F("Settings..."));
  saveGameSettingsToEEPROM();
  delay(1000);
  lcd.clear();
  lcd.print(F("Saved!"));
  delay(1000);
}

/**
 * Processing for the main menu in config mode
 * @return true if we should loop around again
 */
void beginMainMenu() {
  buttons = 0;
  buttonTimer = 0;
  byte selected = loopMenu(MAIN_MENU, 10);
  if(triggerGoBack()){
    return;
  }
  switch(selected) {
    case MM_PLAYER_SETTINGS:
      beginPlayerSettingsMenu();
      break;

    case MM_GAME_SETTINGS:
      beginGameSettingsMenu();
      break;

    case MM_SAVE_PLAYER:
      savePlayerSettings();
      break;

    case MM_SAVE_GAME:
      saveGameSettings();
      break;

    case MM_SAVE_ALL:
      savePlayerSettings();
      saveGameSettings();
      break;

    case GO_BACK_ONE:
      if(! (buttons & BTN_LONG_PRESS) ){
        lcd.clear();
        lcd.print(F("To exit menu"));
        lcd.setCursor(0,1);
        lcd.print(F("Hold Trigger"));
        delay(1000);
      }
  }
}

void MenuManager::begin() {
  turnDisplayOn(DISPLAY_INFINITE);
  lcd.clear();
    lcd.print(F("Main Menu"));
    lcd.setCursor(0,1);
    lcd.print(F("Directions:"));
    buttons = 0;
    while(digitalRead(FUNCTION) == LOW){
      delay(50); //Wait until the user releases the button
    }

  for(byte cv = 0; !menuDelay(1500); cv++) {
    if(cv % 2 == 0){
      lcd.clear();
    } else {
      lcd.setCursor(0,1);
    }
    getMenuInstruction(cv);
    lcd.print(progmemString);
    if(cv==7) {
      cv = -1;
    }
  }
  while(!triggerGoBack()){
    beginMainMenu(); //Loop until user holds trigger to go back
  }
  
}



