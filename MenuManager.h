/**
 * MenuManager.h
 *
 * Manages the display and setting of values for the Duino Tag menu system
 */

#ifndef MENU_MANAGER_H
#define MENU_MANAGER_H

#include "Arduino.h"

 //Store menu instructions in memory
const PROGMEM char menu_empty[] = " "; //For the second line of one line menu items

const PROGMEM char menu_inst_0[] = "Function:";
const PROGMEM char menu_inst_1[] = "Change Value";
const PROGMEM char menu_inst_2[] = "Reload:";
const PROGMEM char menu_inst_3[] = "Select";
const PROGMEM char menu_inst_4[] = "Trigger: Go Back";
const PROGMEM char menu_inst_5[] = "Or Hold To Exit";
const PROGMEM char menu_inst_6[] = "Press Function ";
const PROGMEM char menu_inst_7[] = "To Continue ";
const PROGMEM char* const MENU_INSTRUCTIONS[] = {menu_inst_0, menu_inst_1, menu_inst_2, menu_inst_3, menu_inst_4, menu_inst_5, menu_inst_6, menu_inst_7};

//Store main menu options in memory
const PROGMEM char main_menu_0[] = "Player Settings";
const PROGMEM char main_menu_1[] = "Game Settings";
const PROGMEM char main_menu_5[] = "Save All As";
const PROGMEM char main_menu_3[] = "Default Settings"; //Will also appear as second line for 4 and 2
const PROGMEM char main_menu_2[] = "Save Only Player";
const PROGMEM char main_menu_4[] = "Save Only Game";

const PROGMEM char* const MAIN_MENU[] = {main_menu_0, menu_empty, main_menu_1, menu_empty, main_menu_5, main_menu_3, main_menu_2, main_menu_3, main_menu_4, main_menu_3};

//Store player settings menu options in memory
const PROGMEM char player_menu_0[] = "Player ID";
const PROGMEM char player_menu_2[] = "Team ID";
const PROGMEM char player_menu_4[] = "Power Level";
const PROGMEM char player_menu_6[] = "Role";
const PROGMEM char player_menu_8[] = "Game Version";
const PROGMEM char* const PLAYER_MENU[] = {player_menu_0, menu_empty, player_menu_2, menu_empty, player_menu_4, menu_empty, player_menu_6, 
    menu_empty, player_menu_8, menu_empty};

//Stoe game settings menu options in memory
const PROGMEM char game_menu_0[]  = "Max Health";
const PROGMEM char game_menu_2[]  = "Default Health";
const PROGMEM char game_menu_4[]  = "Max Clips";
const PROGMEM char game_menu_6[]  = "Default Clips";
const PROGMEM char game_menu_8[]  = "Clip Size";
const PROGMEM char game_menu_10[] = "Max RPGs";
const PROGMEM char game_menu_12[] = "Default RPGs";
const PROGMEM char game_menu_14[] = "Max Disable";
const PROGMEM char game_menu_15[] = "Time";
const PROGMEM char game_menu_16[] = "Disable Wait";
const PROGMEM char game_menu_18[] = "Ongoing Chemical";
const PROGMEM char game_menu_19[] = "Burn Damage";
const PROGMEM char game_menu_20[] = "Chemical";
const PROGMEM char game_menu_21[] = "Burn Time";
const PROGMEM char game_menu_22[] = "Ongoing";
const PROGMEM char game_menu_23[] = "Radiation Damge";
const PROGMEM char game_menu_24[] = "Radiation";
const PROGMEM char game_menu_25[] = "Poison Time";
const PROGMEM char game_menu_26[] = "Reload Time";
const PROGMEM char* const GAME_MENU[] = {game_menu_0, menu_empty, game_menu_2, menu_empty, game_menu_4, menu_empty, game_menu_6, 
    menu_empty, game_menu_8, menu_empty, game_menu_10, menu_empty, game_menu_12, menu_empty, game_menu_14, game_menu_15, game_menu_16,
    menu_empty, game_menu_18, game_menu_19, game_menu_20, game_menu_21, game_menu_22, game_menu_23, game_menu_24, game_menu_25, game_menu_26,
    menu_empty};

//Return value to go back one level in menus
const byte GO_BACK_ONE    = 255;

//Return values for main menu selection
const byte MM_PLAYER_SETTINGS = 0;
const byte MM_GAME_SETTINGS   = 2;
const byte MM_SAVE_PLAYER     = 6;
const byte MM_SAVE_GAME       = 8;
const byte MM_SAVE_ALL        = 4;

//Return values for player menu selection
const byte PM_PLAYER_ID     = 0;
const byte PM_TEAM_ID       = 2;
const byte PM_POWER_LEVEL   = 4;
const byte PM_ROLE          = 6;
const byte PM_GAME_VERSION  = 8;

//Return values for game menu selection
const byte GM_MAX_HEALTH    = 0;
const byte GM_DEF_HEALTH    = 2;
const byte GM_MAX_CLIPS     = 4;
const byte GM_DEF_CLIPS     = 6;
const byte GM_CLIP_SIZE     = 8;
const byte GM_MAX_RPG       = 10;
const byte GM_DEF_RPG       = 12;
const byte GM_MAX_DISABLE   = 14;
const byte GM_DISABLE_WAIT  = 16;
const byte GM_ONGOING_CHEM  = 18;
const byte GM_CHEM_TIME     = 20;
const byte GM_ONGOING_RAD   = 22;
const byte GM_RAD_TIME      = 24;
const byte GM_RELOAD_TIME   = 26;

class MenuManager {
public:

  /**
   * Begin processing the menu system
   */
  void begin();

};

#endif //MENU_MANAGER_H