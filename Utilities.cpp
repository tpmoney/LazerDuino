/**
 * Utilities.cpp
 *
 * Utility functions for the Duino Tag Project
 */

#include "Utilities.h"
#include "DuinoTagGlobals.h"
#include "GameParameters.h"
#include <EEPROM.h>
#include <DFRobotDFPlayerMini.h>


void turnDisplayOn(byte timeOut) {
  lcd.display();
  digitalWrite(LCD_BACKLIGHT, HIGH);
  if(timeOut == DISPLAY_INFINITE) {
    displayTimeout = UINT32_MAX;
  } else {
    displayTimeout = millis() + timeOut * SECONDS_TO_MILLIS;
  }
}

void turnDisplayOff() {
  lcd.noDisplay();
  digitalWrite(LCD_BACKLIGHT, LOW);
}

void checkFunctionHold() {
  if(digitalRead(FUNCTION) == LOW) {
    if(buttons & BTN_FUNCTION) { //If the function button was previously being held
      if ((millis() - buttonTimer) / SECONDS_TO_MILLIS >= 3) { //For longer than 3 seconds
        gameFlags |= FLAG_MENU_MODE; //Set the menu mode flag
      }
    } else { //Button was not previously being held
      buttons |= BTN_FUNCTION;
      buttonTimer = millis();
    }
  } else if(buttons & BTN_FUNCTION) { //Button is not being held but it was
    if(millis() - buttonTimer > DEBOUNCE_TIME) {
      buttons &= (~BTN_FUNCTION);
      buttonTimer = 0;
    }
  }
}

void incrementUpToMax(byte &target, byte &amount, byte maxVal) {
  uint16_t newAmount = ((uint16_t) target) + amount;
  if(newAmount > maxVal) {
    target = maxVal;
  } else {
    target = newAmount;
  }
}


void getTeamName(byte teamId) {
  strcpy_P(progmemString, (char *)pgm_read_word(&(TEAMS[teamId])));
}

void getNonPlayerWeaponName(byte weaponId) {
  strcpy_P(progmemString, (char *)pgm_read_word(&(NON_PLAYER_WEAPONS[weaponId])));
}

void playSuccess() {
  audioPlayer.playMp3Folder(SUCCESS_SOUND);
}

void playError() {
  audioPlayer.playMp3Folder(ERROR_SOUND);
}

void playShot() {
  audioPlayer.playMp3Folder(SHOT_SOUND);
}

void playReload() {
  audioPlayer.playMp3Folder(RELOAD_SOUND);
}

void playHit() {
  audioPlayer.playMp3Folder(HIT_SOUND);
}

void reportReceivedByte(byte &byteReceived) { 
  playHit(); //TODO: Do something more useful
}

byte countOnes(uint32_t packetValue) {
  byte count = 0;
  for(byte cv = 0; cv < PACKET_TOTAL_BITS; cv++) {
    if((packetValue & 1) == 1) {
      count++;
    }
    packetValue >>= 1;
  }

  return count;
}

void calculateParity(uint32_t &packet) {
  byte numOnes = countOnes(packet);
  packet <<=1;
  boolean odd = numOnes % 2; 
  if(odd && PS.GAME_TYPE == GAME_A){
    //Odd number of 1s in an even parity game
    // set the parity to 1
    packet |= 1;
  } else if(!odd && PS.GAME_TYPE == GAME_B) {
    //Even number of 1s in an odd parity game
    // set the parity to 1
    packet |= 1;
  }
}

void updateShotPacket() {
  myShotPacket = 0;
  myShotPacket |= PS.TEAM_ID;
  myShotPacket <<= 5;
  myShotPacket |= PS.PLAYER_ID;
  myShotPacket <<= 8;
  myShotPacket |= PS.POWER;
  calculateParity(myShotPacket);

}

/**
 * Write the settings version to EEPROM so we can validate when reading settings back out
 */
void writeConfigHeaderToEEPROM() {
  EEPROM.put(SETTINGS_HEADING_ADDR, SETTINGS_VERSION);
}

/**
 * Validate that the settings version in EEPROM is the same as defined in the compiled code
 */
boolean validateConfigHeaderInEEPROM() {
  char buffer[4];
  EEPROM.get(SETTINGS_HEADING_ADDR, buffer);
  for(uint8_t cv = 0; cv < sizeof(SETTINGS_VERSION); cv++) {
    if(buffer[cv] != SETTINGS_VERSION[cv]) {
      #if DEBUG
        Serial.println(F("Stored heading value did not match"));
        Serial.println(storedVal);
        Serial.println(SETTINGS_VERSION[cv]);
      #endif
      return false;
    }
  }
  return true;
}

/**
 * Save the player setting values in PS to EEPROM for load on startup
 */
void savePlayerSettingsToEEPROM() {
  writeConfigHeaderToEEPROM();
  EEPROM.put(PLAYER_SETTINGS_ADDR, PS);
}

/**
 * Read the player settings out of EEPROM and store in PS
 * @return true if the settings were read in
 */
boolean loadPlayerSettingsFromEEPROM() {
  if(validateConfigHeaderInEEPROM()) {
    EEPROM.get(PLAYER_SETTINGS_ADDR, PS);
    return true;
  }
  return false;
}

/**
 * Save the game setting values in GS to EEPROM for load on startup
 */
void saveGameSettingsToEEPROM() {
  writeConfigHeaderToEEPROM();
  EEPROM.put(GAME_SETTINGS_ADDR, GS);
}

/**
 * Read the game settings out of EEPROM and store in GS
 * @return true if the settings were read
 */
boolean loadGameSettingsFromEEPROM() {
  if(validateConfigHeaderInEEPROM()) {
    EEPROM.get(GAME_SETTINGS_ADDR, GS);
    return true;
  }
  return false;
}

/**
 * Read both the game settings and the player settings
 * out of EEPROM and store them in GS and PS respectively
 * @return true if both settings were read
 */
boolean loadSettingsFromEEPROM() {
  return loadPlayerSettingsFromEEPROM() && loadGameSettingsFromEEPROM();
}