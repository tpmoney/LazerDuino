/**
 * Utilities.h
 *
 * A series of general utility functions used
 * throughout the Duino Tag project
 */

#ifndef UTILITIES_H
#define UTILITIES_H
#include "Arduino.h"

#define SETTINGS_HEADING_ADDR 0   //The address in EEPROM of the settings heading
#define PLAYER_SETTINGS_ADDR  8   //The address in EEPROM of the player settings 
#define GAME_SETTINGS_ADDR    40  //The address in EEPROM of the game settings

/**
 * Turn the LCD display on for a given timeout
 * value. A timeout of 0 means the display
 * will remain on until turnDisplayOff() is called
 */
void turnDisplayOn(byte timeOut);

/**
 * Turn the LCD display off and
 * blank the characters
 */
void turnDisplayOff();

/**
 * Check if the function button is being held
 * and set the flag in "buttons" if it is
 */
void checkFunctionHold();

/**
 * Increment a target value by a given amount, up to a maximum value.
 * Any amount exceeding the maxVal is discared.
 * 
 * @param target the target value to increment
 * @param amount the amount to increment the target by
 * @param maxVal the maximum value the target can be
 */
void incrementUpToMax(byte &target, byte &amount, byte maxVal);

/**
 * Get a team name string out of program memory and
 * copy it to progmemString
 */
void getTeamName(byte teamId);

/**
 * Get a non player weapon name out of program memory and
 * copy it to progmemString
 */
void getNonPlayerWeaponName(byte weaponId);

/**
 * Play a "success" sound
 */
void playSuccess();

/**
 * Play an "error" sound
 */
void playError();

/**
 * Play a "shot" sound
 */
void playShot();

/**
 * Play a reload sound
 */
void playReload();

/**
 * Play a "hit" sound
 */
void playHit();

/**
 * Report a received byte value on the LED/Speaker
 */
void reportReceivedByte(byte &byteReceived);

/**
 * Count the number of "1" bits in a received
 * data packet.
 */
byte countOnes(uint32_t packetValue);

/**
 * Calculate and apply the parity bit for a data packet
 */
void calculateParity(uint32_t &packet);

/**
 * Calculate the shot packet for this player.
 * Since this should only change rarely, we store
 * it in memory to save calculation time when firing.
 * The shot packet format is defined in the fireShot
 * documentation.
 */
void updateShotPacket();

/**
 * Save the player setting values in PS to EEPROM for load on startup
 */
void savePlayerSettingsToEEPROM();

/**
 * Read the player settings out of EEPROM and store in PS
 * @return true if the settings were read in
 */
boolean loadPlayerSettingsFromEEPROM();

/**
 * Save the game setting values in GS to EEPROM for load on startup
 */
void saveGameSettingsToEEPROM();

/**
 * Read the game settings out of EEPROM and store in GS
 * @return true if the settings were read
 */
boolean loadGameSettingsFromEEPROM();

/**
 * Read both the game settings and the player settings
 * out of EEPROM and store them in GS and PS respectively
 * @return true if both settings were read
 */
boolean loadSettingsFromEEPROM();

#endif //UTILITIES_H